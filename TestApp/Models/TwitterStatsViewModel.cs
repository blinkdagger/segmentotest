﻿using System;
using System.Collections.Generic;

namespace TestApp.Models
{
    public class TwitterStatsViewModel
    {
        public TwitterStatsViewModel(IDictionary<string, List<TwitterStat>> dictionary)
        {
            Stats = new Dictionary<string, List<TwitterStat>>(dictionary);
        }

        public TwitterStatsViewModel()
        {
        }

        public Dictionary<string, List<TwitterStat>> Stats { get; set; } = new Dictionary<string, List<TwitterStat>>();
    }

    public class TwitterStat : IComparable<TwitterStat>
    {
        public int Hour { get; set; }
        public int Tweets { get; set; }
        public int Likes { get; set; }
        public int LikesMedian { get; set; }
        public bool HighestMedian { get; set; }
        public bool LowestMedian { get; set; }

        public int CompareTo(TwitterStat other)
        {
            if (other != null)
            {
                return LikesMedian.CompareTo(other.LikesMedian);
            }
            return 1;
        }
    }
}