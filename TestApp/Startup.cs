﻿using Microsoft.Owin;
using Owin;
using TestApp;

[assembly: OwinStartup(typeof(Startup))]

namespace TestApp
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}