﻿using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using LinqToTwitter;
using TestApp.Models;
using Tweetinvi.Core.Extensions;

namespace TestApp.Controllers
{
    [Authorize]
    public class TwitterReportController : Controller
    {
        private static readonly string StatsKey = "stats";

        private static readonly TwitterContext TwitterCtx = new TwitterContext(new SingleUserAuthorizer
        {
            CredentialStore = new SingleUserInMemoryCredentialStore
            {
                ConsumerKey = ConfigurationManager.AppSettings["TwitterKey"],
                ConsumerSecret = ConfigurationManager.AppSettings["TwitterSecret"],
                AccessToken = ConfigurationManager.AppSettings["TwitterAccessToken"],
                AccessTokenSecret = ConfigurationManager.AppSettings["TwitterAccessTokenSecret"]
            }
        });

        // GET: TwitterReport
        public ActionResult Index()
        {
            if (Session[StatsKey] == null)
            {
                Session[StatsKey] = new ConcurrentDictionary<string, List<TwitterStat>>();
            }
            var dict = (ConcurrentDictionary<string, List<TwitterStat>>) Session[StatsKey];
            var model = new TwitterStatsViewModel(dict);
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> AddTwitterUser(string userName)
        {
            if (string.IsNullOrEmpty(userName))
            {
                return RedirectToAction("Index");
            }
            userName = userName.Trim();
            var dict = (ConcurrentDictionary<string, List<TwitterStat>>) Session[StatsKey];
            if (dict.ContainsKey(userName))
            {
                return RedirectToAction("Index");
            }
            bool userExists = await CheckUserExists(userName);
            if (userExists)
            {
                var stats = await GetStatsForUser(userName);
                dict.AddOrUpdate(userName, stats, (s, list) => list);
            }

            return RedirectToAction("Index");
        }

        private async Task<bool> CheckUserExists(string twitterUserName)
        {
            var user = await TwitterCtx.User
                .Where(u => u.Type == UserType.Search && u.Query == twitterUserName)
                .FirstOrDefaultAsync();
            return user != null && user.ScreenNameResponse == twitterUserName;
        }

        private async Task<List<TwitterStat>> GetStatsForUser(string twitterUserName)
        {
            int MaxTweetsToReturn = 200;
            int MaxTotalResults = 1000;

            ulong sinceID = 1;

            // used after the first query to track current session
            ulong maxID;

            var combinedSearchResults = new List<Status>();

            List<Status> tweets =
                await
                    (from tweet in TwitterCtx.Status
                        where tweet.Type == StatusType.User &&
                              tweet.ScreenName == twitterUserName &&
                              tweet.Count == MaxTweetsToReturn &&
                              tweet.SinceID == sinceID &&
                              tweet.TweetMode == TweetMode.Extended
                        select tweet)
                    .ToListAsync();

            if (tweets != null)
            {
                combinedSearchResults.AddRange(tweets);
                ulong previousMaxID = ulong.MaxValue;
                do
                {
                    // one less than the newest id you've just queried
                    maxID = tweets.Min(status => status.StatusID) - 1;

                    previousMaxID = maxID;

                    tweets =
                        await
                            (from tweet in TwitterCtx.Status
                                where tweet.Type == StatusType.User &&
                                      tweet.ScreenName == twitterUserName &&
                                      tweet.Count == MaxTweetsToReturn &&
                                      tweet.MaxID == maxID &&
                                      tweet.SinceID == sinceID &&
                                      tweet.TweetMode == TweetMode.Extended
                                select tweet)
                            .ToListAsync();

                    combinedSearchResults.AddRange(tweets);
                } while (tweets.Any() && combinedSearchResults.Count < MaxTotalResults);
                var result = ProcessTweets(combinedSearchResults);
                return result;
            }
            return new List<TwitterStat>();
        }

        private static List<TwitterStat> ProcessTweets(List<Status> combinedSearchResults)
        {
            var hourAndLike = combinedSearchResults.Select(c => new {c.CreatedAt.Hour, Likes = c.FavoriteCount ?? 0})
                .ToList();
            var grouped = hourAndLike.OrderBy(s => s.Likes)
                .GroupBy(s => s.Hour);
            List<TwitterStat> stats = new List<TwitterStat>(24);
            foreach (var group in grouped)
            {
                var likes = group.Sum(s => s.Likes);
                var hour = group.Key;
                var tweetsCount = group.Count();
                int index = 0;
                int likesMedian;
                if (tweetsCount % 2 == 1)
                {
                    index = tweetsCount / 2;
                    likesMedian = group.ElementAt(index).Likes;
                }
                else
                {
                    index = tweetsCount / 2 - 1;
                    likesMedian = (group.ElementAt(index).Likes + group.ElementAt(index + 1).Likes) / 2;
                }

                stats.Add(new TwitterStat
                {
                    Hour = hour,
                    Likes = likes,
                    LikesMedian = likesMedian,
                    Tweets = tweetsCount
                });
            }
            var max = stats.Max(t => t.LikesMedian);
            var min = stats.Min(t => t.LikesMedian);
            stats.Where(s => s.LikesMedian == max).ForEach(s => s.HighestMedian = true);
            stats.Where(s => s.LikesMedian == min).ForEach(s => s.LowestMedian = true);
            var result = stats.OrderBy(s => s.Hour).ToList();
            return result;
        }
    }
}